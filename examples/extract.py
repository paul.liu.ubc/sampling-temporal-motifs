#!/usr/bin/python

import sys

file = open(sys.argv[1], 'r')

scnt = 0
prev = 0
lines = file.readlines()
results = []
for curr, line in enumerate(lines):
	if '*******' in line:
		scnt += 1
	
	if scnt == 3:
		scnt = 1
		results.append(lines[prev:curr])
		prev = curr
		
curr = len(lines)
results.append(lines[prev:curr])

def extract_result(result):
	get_rt0 = lambda s: s.split()[-1][:-1]
	get_rt1 = lambda s: s.split()[-1]
	
	name 				= result[1].strip()
	mackey 				= get_rt0(result[16].strip())
	mackeySampling 	        	= get_rt0(result[19].strip())
	edges 				= get_rt1(result[24].strip())
	custom				= get_rt1(result[25].strip())
	customPara			= get_rt1(result[32].strip())
	mackeySamplingPara          	= get_rt0(result[48].strip())
	bensonPara			= get_rt0(result[62].strip())
	benson				= get_rt0(result[72].strip())
	
	return '%s,%s,%s,%s,%s,%s,%s,%s,%s' % (name, edges, mackey, mackeySampling, mackeySamplingPara, custom, customPara, benson, bensonPara)
	
print 'Graph,# edges,Backtracking,Backtracking+Sampling,Backtracking+ParallelSampling,BensonLiuMoses+Sampling,BensonLiuMoses+ParallelSampling,Benson,Benson+Parallel'
for result in results:
	print extract_result(result)
