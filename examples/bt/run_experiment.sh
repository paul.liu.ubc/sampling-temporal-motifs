#!/bin/bash

# See motifsamplermain.cpp for parameter descriptions
./motifsamplermain -i:$1 -delta:$2 -nt:$3 -c:$4 -r:$5
