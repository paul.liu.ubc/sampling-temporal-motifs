#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <map>
#include <random>
#include <future>
#include <iomanip>
#include <fstream>

using namespace std;

struct full_edge {
  int src, dst, t;
  bool operator<(const full_edge& o) const {
    if (o.t != t) return t < o.t;
    return make_pair(src, dst) < make_pair(o.src, o.dst);
  }
};

struct half_edge {
  int dst, t;
};

typedef map<int, vector<half_edge>> Graph;

// NOTE: Alter this function to count different 3 edge 2 node motifs.
inline double count_2tmotifs(const vector<pair<int, int>>& subgraph, int delta, int window) {  
  // lets count motifs with 3 edges
  double res = 0;
  for (int i = 0; i < (int) subgraph.size(); i++) {
    if (subgraph[i].second == 0) continue;

    int nfwd = 1, nbk = 0;
    for (int j = i+1; j < (int) subgraph.size(); j++) {
      if (subgraph[j].first - subgraph[i].first > delta) {
        break;
      }
      nfwd += subgraph[j].second;
      nbk += !subgraph[j].second;

      if (subgraph[j].second == 1) {
        int dt = subgraph[j].first - subgraph[i].first;
        res += window * nbk / double(window - dt);
      }
    }
  }

  // count motifs with reversed edge orientation too
  for (int i = 0; i < (int) subgraph.size(); i++) {
    if (subgraph[i].second == 1) continue;

    int nfwd = 1, nbk = 0;
    for (int j = i+1; j < (int) subgraph.size(); j++) {
      if (subgraph[j].first - (int) subgraph[i].first > delta) {
        break;
      }
      nfwd += !subgraph[j].second;
      nbk += subgraph[j].second;

      if (subgraph[j].second == 0) {
        int dt = subgraph[j].first - subgraph[i].first;
        res += window * nbk / double(window - dt);
      }
    }
  }
  return res;
}

// counts the weighted tmotifs, weighted by 1/(tf - ti).
double count_tmotifs(Graph& g, int delta, int window) {
  // counts 2 node motifs
  // pool up all the edges as (u, v, t) with u < v
  map<pair<int, int>, vector<pair<int, int>>> subgraphs;
  for (auto& kv : g) {
    int src = kv.first;
    for (auto e : kv.second) {
      int dst = e.dst, t = e.t;
      if (src < dst) {
        subgraphs[make_pair(src, dst)].push_back(make_pair(t, 1));
      } else {
        subgraphs[make_pair(dst, src)].push_back(make_pair(t, 0));
      }
    }
  }

  double res = 0;
  for (auto& subgraphKV : subgraphs) {
    sort(subgraphKV.second.begin(), subgraphKV.second.end());
    res += count_2tmotifs(subgraphKV.second, delta, window);
  }
  //delete gp;
  return res;
}

int main(int argc, char* argv[]) {
  ios::sync_with_stdio(0);
  cin.tie(0);

  auto res = freopen(argv[1], "r", stdin);
  if (res == nullptr) return 1;
  int delta = atoi(argv[2]);

  double c, mult;
  if (argc > 3) {
	c = atof(argv[3]);
  } else {
  	c = 30;
  }

  if (argc > 4) {
    mult = atof(argv[4]);
  } else {
  	mult = 3e1;
  }
  
  vector<full_edge> edges;
  int u, v, t;
  while (cin >> u >> v >> t) {
    if (u == v) continue;
    edges.push_back({u, v, t});
  }
  sort(edges.begin(), edges.end());
  cerr << "Number of edges: " << edges.size() << endl;

  //outfile is for estimating Ys vector in the paper.
  //ofstream outfile(string(argv[1]) + "-outfile-32.txt", ofstream::out);
  default_random_engine generator;
  uniform_real_distribution<double> distribution(0.0, 1.0);

  srand(0);
  double st = clock();

  const int window = c * delta;

  int ntrials = 1;
  int offset = rand() % window;
  double tot_estimate = 0;
  Graph g;
  //outfile << edges.size() << endl;
  for (int c = 0; c < ntrials; c++) {    
    vector<future<double>> futures;
    vector<double> probs;         
    
    double res = 0;
    int nxt = edges[0].t + offset, idxL = 0, idxR = 0;
    while (idxR < (int) edges.size() && edges[idxR++].t <= nxt);
    while (idxL < (int) edges.size()) {
      int nedges = idxR - idxL;
      
      double pi = min(mult * double(nedges) / edges.size(), 1.0);
      double p = distribution(generator);
      if (p <= pi) {
        for (int i = idxL; i < idxR; i++) {
          auto e = edges[i];
          g[e.src].push_back({e.dst, e.t});
        }

        res += count_tmotifs(g, delta, window) / pi;
      }
      //outfile << count_tmotifs(g, delta, window) << " " << nedges << endl;
      g.clear();

      idxL = idxR;
      if (idxR < (int) edges.size()) {
        int t = edges[idxR].t;
        nxt = ((t-offset)/window) * window + window + offset;
      }
      while (idxR < (int) edges.size() && edges[idxR++].t <= nxt);
    }

    for (int i = 0; i < (int) futures.size(); i++) {
      res += futures[i].get() / probs[i];
    }
    tot_estimate += res;
  }
  cerr << "Time (s): " << (clock() - st) / CLOCKS_PER_SEC << endl;

  double mean = tot_estimate / ntrials;
  cerr << setprecision(3) << fixed << mean << endl;

  return 0;
}