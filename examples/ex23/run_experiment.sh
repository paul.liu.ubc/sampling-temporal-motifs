#!/bin/bash

# ex-23 takes 4 arguments in the following order (motif_file, duration, window multiplier c, sampling multiplier r)
# See paper (section 3) for definition of c and r. ex23 only counts two node three edge motifs, and the specific
# motif counted can be altered by changing the code in the count_2tmotifs function.
./motifsamplermain $1 $2 $4 $5
