#include "temporalmotifsamplerparallel.h"

#include <omp.h>

#include <time.h>
#include <sys/time.h>
double get_wall_time(){
    struct timeval time;
    if (gettimeofday(&time,NULL)){
        //  Handle error
        return 0;
    }
    return (double)time.tv_sec + (double)time.tv_usec * .000001;
}

int main(int argc, char* argv[]) {
  Env = TEnv(argc, argv, TNotify::StdNotify);

  const TStr temporal_graph_filename =
    Env.GetIfArgPrefixStr("-i:", "simple-example.txt",
			  "Input directed temporal graph file");
  const TStr motif_graph_filename =
    Env.GetIfArgPrefixStr("-m:", "simple-motif.txt",
        "Input directed motif graph file");
  const TFlt delta =
    Env.GetIfArgPrefixFlt("-delta:", 4096, "Time window delta");
  const int num_threads =
    Env.GetIfArgPrefixInt("-nt:", 4, "Number of threads for parallelization");
  const TFlt c =
    Env.GetIfArgPrefixFlt("-c:", 30, "Window size multiplier");
  const TFlt mult =
    Env.GetIfArgPrefixFlt("-r:", 30, "Sampling probability multiplier");
  
  omp_set_num_threads(num_threads);
  TempMotifSamplerParallel tmc(temporal_graph_filename, motif_graph_filename);

  Env.PrepArgs(TStr::Fmt("Temporalmotifs. build: %s, %s. Time: %s",
       __TIME__, __DATE__, TExeTm::GetCurTm()));  
  
/*
  TExeTm ExeTm;  
  printf("%d\n", (int) tmc.ExactCountMotifs(delta));
  //printf("%d\n", (int) tmc.BruteCountM2(delta));
  //printf("%d\n", (int) tmc.BruteCountM2ReallySlow(delta));
  printf("\nrun time: %s (%s)\n", ExeTm.GetTmStr(),
   TSecTm::GetCurTm().GetTmStr().CStr());
//*/

  double init;
/*
  init = get_wall_time();
  printf("%lf\n", tmc.ApproximateCountMotifsSlidingWindow(delta));
  printf("\nrun time: %lfs (%s)\n", get_wall_time() - init,
   TSecTm::GetCurTm().GetTmStr().CStr());
//*/

///*
  init = get_wall_time();
  printf("%lf\n", tmc.ApproximateCountMotifsSlidingWindowSkip(delta, c, mult));
  printf("\nrun time: %lfs\n", get_wall_time() - init);
//*/
  return 0;
}
