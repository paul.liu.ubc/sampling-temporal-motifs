#!/bin/bash

declare -a all=("./bt" 
				"./ex23" 
				"./ex23-parallel" 
				"./bt-parallel"
				"./f23-parallel")

declare -a mackey=("./bt" 
				   "./bt-parallel")

declare -a datasets=(
#					 "bitcoin-temporal"
#					 "reddit-reply-temporal"
					 "CollegeMsg"
					 "email-Eu-core-temporal"
#					 "sx-askubuntu"
#					 "sx-mathoverflow"
#					 "sx-stackoverflow"
					 "sx-superuser"
#					 "wiki-talk-temporal"
					)

NUM_THREADS=4
WINDOW_MULT=10
SAMPLE_MULT=30
for f in "${datasets[@]}"
	do
	echo "********************************************************************"
	echo $f
	echo "********************************************************************"
	for i in "${all[@]}"
	do
		echo "-------------------------------------------------------"
		echo "$i"
		echo "-------------------------------------------------------"
		cd "$i"
		bash run_experiment.sh ../test-graphs/$f.txt 86400 $NUM_THREADS $WINDOW_MULT $SAMPLE_MULT
		# Also run the experiments for single threaded exact counts
		if [ "$i" == "./f23-parallel" ]
			then
			bash run_experiment.sh ../test-graphs/$f.txt 86400 1
		fi
		cd -
	done
done
