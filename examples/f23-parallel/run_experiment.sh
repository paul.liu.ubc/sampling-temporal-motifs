#!/bin/bash

# See temporalmotifsmain.cpp for parameter descriptions
./temporalmotifsmain -i:$1 -delta:$2 -nt:$3
