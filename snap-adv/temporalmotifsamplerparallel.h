#ifndef snap_temporalmotifsamplerparallel_h
#define snap_temporalmotifsamplerparallel_h

#include "Snap.h"

#include "temporalmotiftypes.h"

#include <vector>
#include <map>
#include <stack>
#include <iostream>
#include <cassert>
#include <algorithm>
#include <limits>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <random>
#include <thread>
#include <future>

// TODO: no global namespaces
using namespace std;


template<typename R>
  bool is_ready(std::future<R> const& f)
  { return f.wait_for(std::chrono::seconds(0)) == std::future_status::ready; }

// TODO: REPLACE THIS WITH SNAP STRUCTURES
struct TEdge {
	long long src, dst, tim, id;
	const bool operator<(const TEdge& o) const {
		if (o.tim != tim) return tim < o.tim;
		if (o.src != src) return src < o.src;
		if (o.dst != dst) return dst < o.dst;
		if (o.id != id) return id < o.id;
		return false;
	}

	friend ostream& operator<<(ostream& in, const TEdge& o) {
		in << "(" << o.src << "," << o.dst << "," << o.tim << "," << o.id << ")";
		return in;
	}
};

class TempMotifSamplerParallel {
public:
	struct Datum {
		// Vertex based data structures
		vector<int> edgeCount;
    	vector<int> mapGM;
    	vector<int> mapMG;
    	// Edge index based adjacency list
		vector< vector< TEdge > > adj_list;
		vector< vector< TEdge > > revadj_list;
		vector< unordered_map<int, vector<TEdge> > > adjMap;

		void InitializeStructures(vector< TEdge >& edgeList, int Vm) {
			unordered_map<int, int> remap;
			int id = 0;
			for (auto e : edgeList) {
				if (!remap.count(e.src)) {
					remap[e.src] = id++;
				}
				if (!remap.count(e.dst)) {
					remap[e.dst] = id++;
				}
			}
			for (auto& e : edgeList) {
				e.src = remap[e.src];
				e.dst = remap[e.dst];
			}
			sort(edgeList.begin(), edgeList.end());

			for (int i = 0; i < (int) edgeList.size(); i++) {
				edgeList[i].id = i;
			}

			adj_list.resize(id);
			revadj_list.resize(id);
			adjMap.resize(id);
			for (const auto& edge : edgeList) {
				adj_list[edge.src].push_back(edge);
				revadj_list[edge.dst].push_back(edge);
				adjMap[edge.src][edge.dst].push_back(edge);
			}
			edgeCount.resize(id, 0);
			mapGM.resize(id, -1);
			mapMG.resize(Vm, -1);
		} 
	};

    TempMotifSamplerParallel(const TStr& filenameG, const TStr& filenameM);

    static double ExactEnumerateMotifs(
    	int delta, 
    	int window, 
    	vector<TEdge>* edgesP,
    	int Vm,
    	vector< pair<int, int> > edgesM) {

    	vector<TEdge>& edges = *edgesP;
    	Datum data;
    	data.InitializeStructures(edges, Vm);

    	double res = 0;
    	vector<int> eStack;

    	int eG = 0, eM = 0, uG = -1, vG = -1, uM = -1, vM = -1;
    	const int INF = numeric_limits<int>::max();
    	int _t = INF;
    	while (true) {
    		int last = eStack.empty() ? -1 : eStack.back();
    		eG = FindNextMatch(eM, eG, data, _t, edges, last, edgesM);
    		TEdge edge = {0, 0, 0, INF};
    		if (eG < (int) edges.size()) {
    			if (eM == int(edgesM.size()) - 1) {
    				// Apply the weight function
    				if (window == -1) res += 1;
    				else res += 1. / (1 - double(edges[eStack.back()].tim - edges[eStack[0]].tim) / window);
    			} else {
    				edge = edges[eG];
    				uG = edge.src, vG = edge.dst;
    				uM = edgesM[eM].first, vM = edgesM[eM].second;

    				data.mapGM[uG] = uM;
    				data.mapGM[vG] = vM;
    				data.mapMG[uM] = uG;
    				data.mapMG[vM] = vG;

    				data.edgeCount[uG] += 1;
    				data.edgeCount[vG] += 1;
    				if (eStack.empty()) {
    					_t = edge.tim + delta;
    				}
    				eStack.push_back(eG);
    				eM += 1;
    			}
    		}
    		eG += 1;

    		while (eG >= (int) edges.size() || edge.tim > _t) {
    			if (!eStack.empty()) {
    				eG = eStack.back() + 1;
    				eStack.pop_back();

    				edge = edges[eG - 1];
    				uG = edge.src, vG = edge.dst;
    				uM = edgesM[eM].first, vM = edgesM[eM].second;

    				if (eStack.empty()) {
    					_t = INF;
    				}
    				data.edgeCount[uG] -= 1;
    				data.edgeCount[vG] -= 1;

    				if (data.edgeCount[uG] == 0) {
    					uM = data.mapGM[uG];
    					data.mapMG[uM] = -1;
    					data.mapGM[uG] = -1;
    				}

    				if (data.edgeCount[vG] == 0) {
    					vM = data.mapGM[vG];
    					data.mapMG[vM] = -1;
    					data.mapGM[vG] = -1;
    				}

    				eM -= 1;
    			} else {
    				delete edgesP;
    				return res;
    			}
    		}
    	}
    }

    static inline int FindNextMatch(
    	int eM, 
    	int eG, 
    	Datum& data, 
    	int _t,
    	vector<TEdge>& edges,
    	int last,
    	vector< pair<int, int> >& edgesM) {

    	int uM, vM, uG, vG;
    	uM = edgesM[eM].first;
    	vM = edgesM[eM].second;
    	uG = data.mapMG[uM];
    	vG = data.mapMG[vM];

    	vector<TEdge>* S;
    	int head = 0;
    	if (uG >= 0 && vG >= 0) {
    		S = &data.adjMap[uG][vG];
    	} else if (uG >= 0) {
    		S = &data.adj_list[uG];
    	} else if (vG >= 0) {
    		S = &data.revadj_list[vG];
    	} else {
    		S = &edges;
    	}
    	bool small = (S->size() < 16);
    	if (!small) {
	    	head = lower_bound(S->begin(), S->end(), edges[eG]) - S->begin();
	    }

    	for (int i = head; i < (int) S->size(); i++) {
    		auto edge = (*S)[i];
    		if (edge.id < eG || edge.tim > _t) {
    			if (small) continue;
    			else break;
    		}
    		if (last != -1 && edge.tim <= edges[last].tim) {
    			continue;
    		}

    		int _eG = edge.id;
    		int _uG = edge.src, _vG = edge.dst;
    		if (uG == _uG || (uG < 0 && data.mapGM[_uG] < 0)) {
    			if (vG == _vG || (vG < 0 && data.mapGM[_vG] < 0)) {
    				return _eG;
    			}
    		} 
    	}
    	return edges.size();
    }

    TUInt64 ExactCountMotifs(int delta) {
    	vector<TEdge>* tmp = new vector<TEdge>(edges_);
    	return ExactEnumerateMotifs(delta, -1, tmp, Vm_, edgesM_);
    }

    ///*
    double ApproximateCountMotifsSlidingWindowSkip(int delta, double c = 3e1, double mult = 3e1) {
        srand(time(nullptr));

        default_random_engine generator;
        uniform_real_distribution<double> distribution(0.0, 1.0);
        
        const int window = c * delta;
        const int n_trials = 1;

        double tot_estimate = 0;
        for (int c = 0; c < n_trials; c++) {
            int offset = rand() % window;

            vector<future<double>> futures;
            vector<double> probs;

            double res = 0;
            int nxt = edges_[0].tim + offset, idxL = 0, idxR = 0;
            while (idxR < (int) edges_.size() && edges_[idxR++].tim <= nxt);
            while (idxL < (int) edges_.size()) {
                int nedges = idxR - idxL;

                double pi = min(mult * double(nedges) / edges_.size(), 1.0);
                double p = distribution(generator);
                if (p <= pi) {
                    vector<TEdge>* tmp = new vector<TEdge>();
                    tmp->assign(edges_.begin() + idxL, edges_.begin() + idxR);
                    futures.push_back( 
                        async(launch::async,
                              ExactEnumerateMotifs,
                              delta, window, tmp, Vm_, edgesM_)
                    );
                    probs.push_back(pi);
                }

                idxL = idxR;
                if (idxR < (int) edges_.size()) {
                    int t = edges_[idxR].tim;
                    nxt = ((t-offset)/window) * window + window + offset;
                }
                while (idxR < (int) edges_.size() && edges_[idxR++].tim <= nxt);
            }

            for (int i = 0; i < (int) futures.size(); i++) {
                res += futures[i].get() / probs[i];
            }
            tot_estimate += res;
        }
        return tot_estimate / n_trials;
    }
    //*/

    double ApproximateCountMotifs(int delta) {
    	return ApproximateCountMotifsSlidingWindowSkip(delta);
    }

private:
	vector< TEdge > edges_;
	vector< pair<int, int> > edgesM_;
	int Vm_;
};

#endif  // snap_temporalmotifsamplerparallel_h
