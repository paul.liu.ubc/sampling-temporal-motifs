### Sampling methods for counting temporal motifs

This respository contains a C++ proof of concept for the ideas presented in the paper [Sampling methods for counting temporal motifs](https://arxiv.org/abs/1810.00980). The code is self-contained and is built using components from the [Stanford SNAP project](http://snap.stanford.edu/). Most of the datasets used in the paper can be found [here](https://snap.stanford.edu/temporal-motifs/data.html). The Reddit dataset can be found [here](http://www.cs.cornell.edu/~arb/data/temporal-reddit-reply/index.html). 

## For Linux

Here is an example of how to use the code:
```
# Download the code
git clone git@gitlab.com:paul.liu.ubc/sampling-temporal-motifs.git

# Build the code
make
cd examples

# Run all algorithms
./run_experiments.sh
```

The constants referenced in the paper can be tuned in `run_experiments.sh`.

## For MacOS
The above instructions still apply, but openmp must be installed first (see [here](https://iscinumpy.gitlab.io/post/omp-on-high-sierra/)).


### Using each algorithm individually
The code for three algorithms used in the paper can be found in `./examples/bt-parallel/`, `./examples/ex23-parallel/` and `./examples/f23-parallel/`. For an example of how to use the code, see the `run_experiment.sh` in each folder.